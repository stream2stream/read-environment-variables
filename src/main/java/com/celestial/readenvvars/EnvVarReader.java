/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.readenvvars;

import com.celestial.dbutils.DBConnector;
import java.util.Map;
import  com.celestial.loginmanager.ApplicationScopeHelper;
import  com.celestial.dbutils.User;
import com.celestial.dbutils.UserHandler;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;



/**
 *
 * @author Selvyn
 */
public class EnvVarReader
{
    public static void main (String[] args) 
    {
        Map<String, String> env = System.getenv();
        /*
        for (String envName : env.keySet()) 
        {
            System.out.format("%s=%s%n",
                              envName,
                              env.get(envName));
        }
        */
        String value = env.get("MYSQL_USER");
        System.out.println( "MYSQL_USER = " + value );
        
        ApplicationScopeHelper ash = new ApplicationScopeHelper();
        ash.bootstrapDBConnection();
        
        System.out.println( "Adding user: " + value );
        User  theUser = new User(value, "99999999");
        UserHandler uh = UserHandler.getLoader();
        try
        {
            uh.writeUserToDB(DBConnector.getConnector(), theUser);
        } catch (IOException ex)
        {
            Logger.getLogger(EnvVarReader.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}